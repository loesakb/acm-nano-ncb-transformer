# base image
FROM registry.access.redhat.com/ubi8/ubi:latest

# install required tools and libs
RUN yum update -y && \
    yum install -y \
    nginx \
    python36 \
    vim curl \
    poppler-utils libSM

# nginx config
COPY data/nginx.conf /etc/nginx/conf.d/default.conf

RUN cp /usr/bin/python3 /usr/bin/python

# set working dir
WORKDIR /app

# copy dependencies file and install
COPY requirements.txt .
RUN pip3 install -U pip && \
    pip install wheel && \
    pip install setuptools && \
    pip install -r requirements.txt

# environment variables
# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE 1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED 1
ENV CONFIGURATION_FILE=/app/data/application.cfg
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8
ENV PYTHONPATH="${PYTHONPATH}:/app/src/lib/"

# generate version.txt
RUN echo "{\"name\":\"python-api-poc\",\"version\":\"0.1-dev\"}" > version.txt

RUN mkdir /tmp/prometheus_multiproc
# copy app
ADD . /app
EXPOSE 5000
# start
# CMD ["flask", "run", "--host", "0.0.0.0", "-cl-port", "5000"]
CMD ["./start.sh"]
# CMD ["gunicorn", "--reload", "--bind", "0.0.0.0:5000", "'src:create_app()'"]
