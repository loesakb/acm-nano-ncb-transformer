## Installation(For Development)

Install python lib from requirements.txt cmd
```sh
pip3 install -r requirements.txt
```

How to run project
```sh
export CONFIGURATION_FILE={project_path}/configuration-dev.cfg
export FLASK_APP=src:create_app
export FLASK_ENV=development
flask run
```

Configuration file
```sh
APP_NAME="acm-nano-ncb-transformer"

LOG_LEVEL="DEBUG"
LOG_FORMAT=f"%(asctime)s | %(levelname)s | NNO-TH | APP_LOG | {APP_NAME} | %(correlation_id)s | %(remote_addr)s | PYTHON | %(message)s"
LOG_TIMEZONE="Asia/Bangkok"

NCRS_URL="http://192.168.251.2/ncrs/servlet/xmladapter"
RETURN_ONLYRESPONSECODE=True
```
Meaning of RETURN_ONLYRESPONSECODE is use for return response code only.

> * RETURN_ONLYRESPONSECODE = True
> 
>   * The API will do only validate fields. Not call NCRS to get xml
> 
>   * Success Case: The API will return HTTP CODE 200. If validate fields is success. 
> 
>   * Fail Case: The API will return HTTP CODE 400. If validate fields is fail.
> 
> * RETURN_ONLYRESPONSECODE = False
> 
>   * The API will validate fields and call NCRS to get xml and convert xml to json and return json body of ncrs.
> 
>   * Success Case: The API will return HTTP CODE 200. If validate fields is success. 
> 
>   * Fail Case: The API will return HTTP CODE 400. If validate fields is fail.
> 
> * After change configuration file. please stop and run cmd flask run again.