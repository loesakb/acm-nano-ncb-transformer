import multiprocessing
from prometheus_flask_exporter.multiprocess import GunicornPrometheusMetrics

bind = "127.0.0.1:8000"
workers = 4
worker_class = "gevent"
timeout = 120


def when_ready(server):
    GunicornPrometheusMetrics.start_http_server_when_ready(8001)


def child_exit(server, worker):
    GunicornPrometheusMetrics.mark_process_dead_on_child_exit(worker.pid)
