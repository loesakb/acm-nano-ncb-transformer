import os
import sys

from flask import Flask
from src.model.api_response import APIResponse


def create_app(config=None):
    add_lib_path()

    # create and configure the app
    app = Flask(__name__)
    app.config['JSON_SORT_KEYS'] = False
    if config is None:
        app.config.from_envvar('CONFIGURATION_FILE', silent=False)
    else:
        app.config.from_mapping(config)

    from . import logging
    logging.init(app)

    from . import http_handler
    http_handler.init(app)
    register_blueprint(app)
    if not app.config.get('TESTING', False):
        # prometheus metrics
        from . import metric
        metric.init(app)
        # register more default metrics (must be after register all routes)
        metric.register_more_defaults(app)

    return app


def register_blueprint(app):
    # from src.controller import hello_controller
    # app.register_blueprint(hello_controller.bp, url_prefix="/hello")

    from src.controller import util_controller
    app.register_blueprint(util_controller.bp, url_prefix="/")

    from src.controller import transform_controller
    app.register_blueprint(transform_controller.bp, url_prefix="/v1")


def add_lib_path():
    c_libpath = os.path.join(os.getcwd(), 'src', 'lib')
    if c_libpath not in sys.path:
        sys.path.append(c_libpath)
