FIELD_INVALID = '{} is invalid'
FIELD_MISSING = '{} is missing'
DOCUMENT_ID_NOT_FOUND = 'document_id: {} not found'
CONFIG_KEY_NOT_EXIST = 'Configuration key {} does not exist.'
DOCUMENT_ID_LIMIT = 'document_id more than limit. limit is {}'
PYTHON_LIB_ERROR = 'Python lib exception. Reason : {}'