from enum import Enum


class ResponseEnum(Enum):
    STATUS_PWP2000 = ("acm-nano-python-wrapper-service",
                      200, "PWP2000", "Success", "Success")
    STATUS_PWP4000 = ("acm-nano-python-wrapper-service", 400, "PWP4000",
                      "Bad request", "Invalid request, invalid request JSON format or httpHeader")
    STATUS_PWP4010 = ("acm-nano-python-wrapper-service", 401,
                      "PWP4010", "Unauthorized", "Invalid credential")
    STATUS_PWP4040 = ("acm-nano-python-wrapper-service", 404,
                      "PWP4040", "Data Not found", "Data Not found")
    STATUS_PWP5000 = ("acm-nano-python-wrapper-service", 500,
                      "PWP5000", "Internal server error", "Internal server error")
    STATUS_PWP5040 = ("acm-nano-python-wrapper-service", 504,
                      "PWP5040", "Timeout", "System timeout, please try again later")
    STATUS_PWP8001 = ("acm-nano-python-wrapper-service", 409,
                      "PWP8001", "Resource conflict", "business validation failed")
    STATUS_PWP8002 = ("acm-nano-python-wrapper-service", 409,
                      "PWP8002", "Resource conflict",
                      "Duplicate document_id")
    STATUS_PWP8003 = ("acm-nano-python-wrapper-service", 409,
                      "PWP8003", "Resource conflict",
                      "Document date is invalid")


    def __init__(self, type, status_code, code, message, description):
        self.type = type
        self.status_code = status_code
        self.code = code
        self.message = message
        self.description = description
