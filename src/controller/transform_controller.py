from flask import Blueprint, request, jsonify, json
from CAL_NCB_TRANSFORM import calNCBTransfrom
from src.decorator.auth_decorator import auth_required
bp = Blueprint('transform', __name__)


@bp.before_request
@auth_required
def bp_before_request():
    """check access before request"""


@bp.route('/transform', methods=['POST'])
def inquiry():
    json_data = json.loads(request.data)
    json_resp = calNCBTransfrom(json_data)
    return jsonify(json_resp[0])
