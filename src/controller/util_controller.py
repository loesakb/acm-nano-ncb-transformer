from flask import Blueprint, jsonify, current_app, request, abort, json
from src.exception.handler_exception import abort_bad_request
from src.constant import error_message_template

bp = Blueprint('utils', __name__)


@bp.route('/info', methods=['GET'])
def info():
    appinfo = open('version.txt', 'r').read()
    response = json.loads(appinfo)
    return jsonify(response)


@bp.route('/health', methods=['GET'])
def health():
    return jsonify({'status': 'UP'})


@bp.route('/prometheus', methods=['GET'])
def prometheus():
    return '';


@bp.route('/config', methods=['GET'])
def config():
    result = {}
    keys = request.args.getlist("key")

    if not keys:
        result = json.loads(json.dumps(current_app.config, default=str))
    else:
        for key in keys:
            if key not in current_app.config:
                abort_bad_request(error_message_template.CONFIG_KEY_NOT_EXIST.format(key))
            else:
                result[key] = current_app.config[key]

    return jsonify(result)

