from functools import wraps
from flask import request, current_app
from src.exception.handler_exception import abort_unauthorized
from src.constant.response_enum import ResponseEnum


def auth_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if request.headers.get('X-Access-Key') and request.headers.get('X-Access-Key') == current_app.config.get('ACCESS_KEY', ''):
            return f(*args, **kwargs)
        else:
            abort_unauthorized()
    return decorated_function
