from src.constant.response_enum import ResponseEnum


class BadRequestException(Exception):

    def __init__(self, description=None):
        self.code = ResponseEnum.STATUS_PWP4000.status_code
        self.error_code = ResponseEnum.STATUS_PWP4000.code
        self.message = ResponseEnum.STATUS_PWP4000.message
        self.description = not description and ResponseEnum.STATUS_PWP4000.description or description
        super().__init__(description)


class UnauthorizedException(Exception):

    def __init__(self, message=None, description=None):
        self.code = ResponseEnum.STATUS_PWP4010.status_code
        self.error_code = ResponseEnum.STATUS_PWP4010.code
        self.message = not message and ResponseEnum.STATUS_PWP4010.message or message
        self.description = not description and ResponseEnum.STATUS_PWP4010.description or description
        super().__init__(message)

class BusinessException(Exception):
     def __init__(self, enum, message=None, description=None):
        self.code = enum.status_code
        self.error_code = enum.code
        self.message = not message and enum.message or message
        self.description = not description and enum.description or description
        super().__init__(message)

def abort_bad_request(description = None):
    raise BadRequestException(description)

def abort_unauthorized(message = None, description = None):
    raise UnauthorizedException(message, description)

def abort_besiness(enum, message=None, description=None):
    raise BusinessException(enum, message, description)