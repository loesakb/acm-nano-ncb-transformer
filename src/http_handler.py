from flask import jsonify, request, g
from werkzeug.exceptions import HTTPException

from src.model.api_response import APIErrorResponse
from src.util.request_util import create_correlation_id
from src.constant import constant
from src.constant.response_enum import ResponseEnum
from src.exception.handler_exception import UnauthorizedException, BadRequestException, BusinessException

import json

def init(app):
    @app.before_request
    def before_request():
        """ This function handles HTTP requests arriving the API """
        """set correlation id to request context"""
        correlation_id = request.headers.get(constant.X_CORRELATION_ID, '')
        if correlation_id == '':
            correlation_id = create_correlation_id(
                app.config.get('CORRELATION_PREFIX', 'DER'))

        g.correlationId = correlation_id

        log_params = [
            ('method', request.method),
            ('path', request.path),
            ('headers', dict(request.headers)),
            ('parameters', dict(request.args)),
        ]

        if request.data is not None:
            try:
                json_body = json.loads(request.data)
                log_params.append(('body', json_body))
            except:
                log_params.append(('body', request.data))

        log_parts = []
        for name, value in log_params:
            log_parts.append(f"{name}=[{value}]")
        log_line = "LOGGING_INTERCEPTOR :: logType=[REQUEST] " + " ".join(
            log_parts)
        if (request.path != "/health") and (request.path != "/info"):
            app.logger.info(log_line)

    @app.after_request
    def after_request(response):
        """ This function handles HTTP responses to client """
        log_params = [
            ('method', request.method),
            ('path', request.path),
            ('httpStatus', response.status_code),
            ('headers', dict(response.headers)),
            ('body', response.json)
        ]
        log_parts = []
        for name, value in log_params:
            log_parts.append(f"{name}=[{value}]")
        log_line = "LOGGING_INTERCEPTOR :: logType=[RESPONSE] " + " ".join(
            log_parts)
        if (request.path != "/health") and (request.path != "/info"):
            app.logger.info(log_line)

        response.headers[constant.X_CORRELATION_ID] = g.get(
            'correlationId', '')

        return response

    @app.errorhandler(BusinessException)
    def business_exception(error):
        app.logger.error(error)
        error_code = error.code
        error_response = APIErrorResponse(
            code=error.error_code, msg=error.message, desc=error.description)
        return jsonify(error_response.to_dict()), error_code

    @app.errorhandler(BadRequestException)
    def bad_request_exception(error):
        app.logger.error(error)
        error_code = error.code
        error_response = APIErrorResponse(
            code=error.error_code, msg=error.message, desc=error.description)
        return jsonify(error_response.to_dict()), error_code

    @app.errorhandler(UnauthorizedException)
    def unauthorized_exception(error):
        app.logger.error(error)
        error_code = error.code
        error_response = APIErrorResponse(
            code=error.error_code, msg=error.message, desc=error.description)
        return jsonify(error_response.to_dict()), error_code

    @app.errorhandler(HTTPException)
    def http_exception(error):
        app.logger.error(error)
        error_code = error.code
        if 404 == error_code:
            error_response = APIErrorResponse(
                code=ResponseEnum.STATUS_PWP4040.code,
                msg="not found",
                desc=str(error))
        else:
            error_response = APIErrorResponse(
                code=ResponseEnum.STATUS_PWP5000.code,
                msg=ResponseEnum.STATUS_PWP5000.message,
                desc=str(error))
        return jsonify(error_response.to_dict()), error_code

    @app.errorhandler(Exception)
    def unhandled_exception(error):
        app.logger.error(f"Unhandled Exception: {str(error)}")
        error_code = ResponseEnum.STATUS_PWP5000.status_code
        error_response = APIErrorResponse(enum=ResponseEnum.STATUS_PWP5000)
        return jsonify(error_response.to_dict()), error_code
