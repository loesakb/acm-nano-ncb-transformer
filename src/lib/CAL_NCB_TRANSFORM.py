import json
import xmltodict


def calNCBTransfrom(ncrs_data):
    json_output = '{"tracking_id":"","datetime":"","no_of_npl":"","no_of_litigation":"","no_of_moratorium":"",' \
                  '"no_of_restructure":"","no_of_debt_clinic":"","ncb_score":"","ncb_proxy_income":"",' \
                  '"ncb_proxy_income_type":"","ncb_response_date":"","no_of_max_rc_dlq":"","no_of_max_sv_dlq":"",' \
                  '"no_of_max_fq_dlq":"","no_of_enq_3m":"","no_of_enq_6m":"","no_of_enq_9m":"","no_of_enq_12m":"",' \
                  '"no_of_institute":"","bankruptcy_flag":"","max_card_limit":"","max_pl_limit":"","no_of_hl":"",' \
                  '"no_of_hl_6m":"","no_of_pl":"","no_of_cc":"","no_of_hp":"","total_acct":"","total_acct_active":"",' \
                  '"total_acct_cls":"","total_acct_cls_1y":"","total_acct_cls_2y":"","no_of_pl_bot":"",' \
                  '"no_of_dgl":"","debt_amt_com":"","debt_amt_hl":"","debt_amt_od":"","debt_amt_pl":"",' \
                  '"debt_amt_pl_bot":"","debt_amt_cc":"","debt_amt_hp":"","total_debt_amt":"","debt_amt_tl":"",' \
                  '"debt_amt_rev":"","no_of_cc_6m":"","total_os":"","min_open_date":"","total_credit_limit":"",' \
                  '"potential_skip_pmt":"","no_of_cc_trans":"","no_of_cc_rev":"","credit_card_flag":"",' \
                  '"zipcode_reg":"","zipcode_hom":"","zipcode_wrk":"","no_of_name":"","max_account_per":"",' \
                  '"avg_account_per":""} '

    return json.loads(json_output)
