import datetime
import logging
import sys
import pytz
import tzlocal
from flask import has_request_context, request, g


class CustomLogFormatter(logging.Formatter):
    def __init__(self, log_timezone, *args, **kwargs):
        self.log_timezone = log_timezone
        super().__init__(*args, **kwargs)

    def converter(self, timestamp):
        dt = datetime.datetime.fromtimestamp(timestamp)
        tzinfo = tzlocal.get_localzone()
        dt_local = tzinfo.localize(dt)
        return dt_local.astimezone(pytz.timezone(self.log_timezone))

    def formatTime(self, record, datefmt=None):
        dt = self.converter(record.created)
        if datefmt:
            s = dt.strftime(datefmt)
        else:
            try:
                s = dt.isoformat(timespec='milliseconds')
            except TypeError:
                s = dt.isoformat()
        return s

    def format(self, record):
        if has_request_context():
            record.correlation_id = g.get('correlationId', '')
            record.remote_addr = request.headers.get('X-Forwarded-For', request.remote_addr)
            record.path = request.path
        else:
            record.correlation_id = ''
            record.remote_addr = ''
            record.path = ''

        return super().format(record)


def init(app):
    # create logger
    logger = logging.getLogger()
    log_level = log_level_mapping(app.config.get('LOG_LEVEL', "INFO"))
    logger.setLevel(log_level)

    # create console handler and set level to debug
    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(log_level)

    # create formatter
    formatter = CustomLogFormatter(
        app.config.get('LOG_TIMEZONE', 'UTC'),
        app.config.get('LOG_FORMAT'),
        app.config.get('LOG_DATETIME_FORMAT'))

    # add formatter to ch
    ch.setFormatter(formatter)

    # add ch to logger
    logger.addHandler(ch)


def log_level_mapping(level):
    level = level.upper()
    if level == "ERROR":
        return logging.ERROR
    if level == "WARNING":
        return logging.WARNING
    if level == "INFO":
        return logging.INFO

    return logging.DEBUG