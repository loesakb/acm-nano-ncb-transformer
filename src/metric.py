from flask import request
import prometheus_flask_exporter
from prometheus_flask_exporter.multiprocess import GunicornPrometheusMetrics

metrics = GunicornPrometheusMetrics.for_app_factory(
    defaults_prefix=prometheus_flask_exporter.NO_PREFIX)


def init(app):
    metrics.init_app(app)


def register_more_defaults(app):
    # register more default metrics
    metrics.register_default(
        metrics.counter(
            'http_request_count_by_path', 'Request count by request paths',
            labels={'path': lambda: request.path}
        ),
        app=app
    )
    metrics.register_default(
        metrics.counter(
            'http_request_count_by_endpoint', 'Request count by endpoints',
            labels={'endpoint': lambda: request.endpoint}
        ),
        app=app
    )
