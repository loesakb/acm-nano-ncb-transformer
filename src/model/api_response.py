import time
from json import JSONEncoder
from src.constant.response_enum import ResponseEnum


class APIResponse:
    def __init__(self, data):

        self.status = {
            "code": ResponseEnum.STATUS_PWP2000.code,
            "message": ResponseEnum.STATUS_PWP2000.message,
            "description": ResponseEnum.STATUS_PWP2000.description
        }
        self.data = data

    def to_dict(self):
        return self.__dict__


class APIErrorResponse:
    def __init__(self, **kwargs):
        enum = kwargs.get('enum', None)
        if not enum:
            code = kwargs.get('code', '')
            msg = kwargs.get('msg', '')
            desc = kwargs.get('desc', '')
            self.status = {
                "code": code,
                "message": msg,
                "description": desc
            }
        else:
            self.status = {
                "code": enum.code,
                "message": enum.message,
                "description": enum.description
            }
            

    def to_dict(self):
        return self.__dict__
