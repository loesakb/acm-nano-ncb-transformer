from src.util.common_util import validate_email_address


class NcrsRequest:
    def __init__(self, member_ref, consent, first_name, family_name, date_of_birth, id_type, id_number, email, witness):
        self.member_ref = member_ref
        self.consent = consent
        self.first_name = first_name
        self.family_name = family_name
        self.date_of_birth = date_of_birth
        self.id_type = id_type
        self.id_number = id_number
        self.email = email
        self.witness = witness

    def validate_consent(self):
        if self.consent == 'N':
            return False
        else:
            return True

    def validate_empty_value(self):
        if (not self.member_ref) and (not self.consent) and (not self.first_name) and (not self.family_name) \
                and (not self.date_of_birth) and (not self.id_type) and (not self.id_number) and (not self.email):
            return False
        else:
            return True
