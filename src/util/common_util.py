import re
from xml.etree import ElementTree
from xml.dom import minidom

regex_email = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'


def validate_email_address(email):
    if re.match(regex_email, email):
        return True
    else:
        return False


def check_if_duplicates(list_elem):
    if len(list_elem) == len(set(list_elem)):
        return False
    else:
        return True


def xml_prettify(elem):
    """Return a pretty-printed XML string for the Element.
        """
    rough_string = ElementTree.tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="  ")