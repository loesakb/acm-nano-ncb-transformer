import uuid
import requests
from flask import g, current_app
from src.constant import constant


def create_correlation_id(prefix):
    uuid_str = str(uuid.uuid4()).replace("-", "").lower()
    return (prefix + '-' + uuid_str)[:32]


def request_get(url, headers=None):
    if headers is None:
        headers = {}

    if g.__bool__():
        headers.update({constant.X_CORRELATION_ID: g.get('correlationId', '')})

    log_request('GET', url, headers)
    response = requests.get(url, headers=headers)
    log_response('GET', url, response.headers)
    return response


def log_request(method, url, headers, body=None):
    log_rest_interceptor('REQUEST', method, url, headers, body)


def log_response(method, url, headers, body=None):
    log_rest_interceptor('RESPONSE', method, url, headers, body)


def log_rest_interceptor(type, method, url, headers, body=None, ):
    if body == None:
        body = {}
    log_params = [
        ('method', method),
        ('path', url),
        ('headers', dict(headers)),
        ('body', dict(body))
    ]
    log_parts = []
    for name, value in log_params:
        log_parts.append(f"{name}=[{value}]")
    log_line = f'REST_INTERCEPTOR :: logType=[{type}] {" ".join(log_parts)}'
    if current_app.__bool__():
        current_app.logger.debug(log_line)
