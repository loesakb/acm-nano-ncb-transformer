#!/bin/bash

export prometheus_multiproc_dir=/tmp/prometheus_multiproc
if [ ! -d "$prometheus_multiproc_dir" ]; then
    mkdir "$prometheus_multiproc_dir"
fi
rm -f "$prometheus_multiproc_dir"/*.db
gunicorn -c manage.py "src:create_app()" & nginx -g "daemon off;"

