import urllib.request
import requests
import json
import xmltodict


def ncrs_inquiry():
    url = "http://192.168.251.2/ncrs/servlet/xmladapter"
    xml_data = """
<ncrsrequest>
   <header>
      <user>system</user>
      <password>password12</password>
      <command>BB01001</command>
   </header>
   <body>
      <tuefenquiry>
         <header>
            <memberref>202010E1623742280</memberref>
            <enqpurpose>01</enqpurpose>
            <enqamount>0</enqamount>
            <consent>Y</consent>
            <disputeenquiry>Y</disputeenquiry>
            <creditscore>Y</creditscore>
         </header>
         <name>
            <firstname>สมมุติ1</firstname>
            <familyname>ทดสอบ</familyname>
            <dateofbirth>19840529</dateofbirth>
         </name>
         <id>
            <idtype>01</idtype>
            <idnumber>3100800087082</idnumber>
         </id>
      </tuefenquiry>
   </body>
</ncrsrequest>"""
    url = f"{url}?q={xml_data}"
    resp = requests.get(url)
    print(f"Status: {resp.status_code}")
    print(f"Body: {resp.text}")
    print(f"JSON: {json.dumps(xmltodict.parse(resp.text), ensure_ascii=False, indent=4).encode('iso8859')}")


ncrs_inquiry()