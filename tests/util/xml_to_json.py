
import xmltodict
import pprint
import json

my_xml = """
<ncrsresponse>
    <header>
        <user>system</user>
        <password>XXXXXXXX</password>
        <command>BB01001</command>
    </header>
    <body>
        <transaction>
            <name>
                <familyname>ชินพานิชย์</familyname>
                <firstname>อรกานต์</firstname>
                <dateofbirth>19940925</dateofbirth>
            </name>
            <id>
                <idnumber>1779900174935</idnumber>
                <idtype>01</idtype>
            </id>
            <enquiryamount>0</enquiryamount>
            <memberref>201911E000443</memberref>
            <creditscore>Y</creditscore>
            <user>system</user>
            <enquirystatus>EQ</enquirystatus>
            <mediacode>BB</mediacode>
            <consent>Y</consent>
            <trackingid>4611</trackingid>
            <responsedate>20191106 102719</responsedate>
            <enquirydate>20191106 102719</enquirydate>

            <tuefresponse>
                <responsedata></responsedata>
                <header>
                    <memberref>201911E000443</memberref>
                    <subjectreturncode>1</subjectreturncode>
                    <enqcontrolnum>699299465</enqcontrolnum>
                </header>
                <subject>
                    <name>
                        <familyname>ชินพานิชย์</familyname>
                        <firstname>อรกานต์</firstname>
                        <dateofbirth>19940925</dateofbirth>
                        <enqconsent>Y</enqconsent>
                    </name>
                    <id>
                        <idnumber>1779900174935</idnumber>
                        <idtype>01</idtype>
                    </id>
                    <enquiry>
                        <enqpurpose>01</enqpurpose>
                        <currencycode>THB</currencycode>
                        <enqdate>20190920</enqdate>
                        <enqtime>142618</enqtime>
                        <shortname>BANK</shortname>
                    </enquiry>
                    <enquiry>
                        <enqpurpose>02</enqpurpose>
                        <currencycode>THB</currencycode>
                        <enqdate>20190902</enqdate>
                        <enqtime>214312</enqtime>
                        <shortname>CONSUMER FINANCE</shortname>
                    </enquiry>
                    <enquiry>
                        <enqpurpose>01</enqpurpose>
                        <currencycode>THB</currencycode>
                        <enqdate>20190,920</enqdate>
                        <enqtime>142618</enqtime>
                        <shortname>BANK</shortname>
                    </enquiry>
                    <enquiry>
                        <enqpurpose>02</enqpurpose>
                        <currencycode>THB</currencycode>
                        <enqdate>20190902</enqdate>
                        <enqtime>214312</enqtime>
                        <shortname>CONSUMER FINANCE</shortname>
                    </enquiry>
                    <enquiry>
                        <enqpurpose>01</enqpurpose>
                        <currencycode>THB</currencycode>
                        <enqdate>20190614</enqdate>
                        <enqtime>145739</enqtime>
                        <shortname>CONSUMER FINANCE</shortname>
                    </enquiry>
                    <enquiry>
                        <enqpurpose>01</enqpurpose>
                        <currencycode>THB</currencycode>
                        <enqdate>20190404</enqdate>
                        <enqtime>172415</enqtime>
                        <shortname>BANK</shortname>
                    </enquiry>
                    <creditscore>
                        <errorcode></errorcode>
                        <scoreversion>01</scoreversion>
                        <scorename>GE</scorename>
                    </creditscore>
                </subject>
            </tuefresponse>
            <tuefenquiry>
                <header>
                    <enqpurpose>01</enqpurpose>
                    <memberref>201911E000443</memberref>
                    <enqamount>000000000</enqamount>
                    <creditscore>Y</creditscore>
                    <consent>Y</consent>
                    <disputeenquiry>Y</disputeenquiry>
                </header>
                <attribute>
                    <ledbankruptcy>Y</ledbankruptcy>
                </attribute>
                <name>
                    <familyname>ชินพานิชย์</familyname>
                    <firstname>อรกานต์</firstname>
                    <dateofbirth>19940925</dateofbirth>
                </name>
                <id>
                    <idnumber>1779900174935</idnumber>
                    <idtype>01</idtype>
                </id>
            </tuefenquiry>

            <transactiondate>20191106</transactiondate>
            <disputeenquiry>Y</disputeenquiry>
            <enquirypurpose>01</enquirypurpose>

            <ledbankruptcy>
                <memberref>201911E000443</memberref>
                <returncode>0</returncode>
            </ledbankruptcy>

        </transaction>
    </body>
</ncrsresponse>
"""

print(json.dumps(xmltodict.parse(my_xml), indent=4))

